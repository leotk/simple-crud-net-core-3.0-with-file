﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your Javascript code.

function File_changed() {
    var formulario = document.FomuValidar;
    formulario.validador.value = "false"
    formulario.querySelector('[name="respuesta"]').innerHTML = " "; //JUST GET ONE VALUE FIELD
    //document.getElementById("respuesta").innerHTML = " ";
    //formulario.elements["respuesta"].innerHTML = "";
}

function Funcion() {
    var check = document.getElementById("Change");
    var ifile = document.getElementById("Pdfdoc");
    var nombrepdf = document.getElementById("Nombre");
    var boton = document.getElementById("bcargar");

    if (check.checked == true) {
        ifile.style.display = "block"
        boton.style.display = "block"
        nombrepdf.style.display = "none"
    } else {
        ifile.style.display = "none";
        boton.style.display = "none"
        nombrepdf.style.display = "block"
    }
}

function ValidadorFormu(evt) {
    var formulario = document.FomuValidar;
    var condicion = true;

    var fcheck = formulario.elements["chek"];
    if (fcheck) {
        if (formulario.chek.checked == true) {
            if (formulario.Pdfdoc.value.length != 0 && formulario.validador.value == "false") {
                alert("El archivo debe ser validado");
                condicion = false;
            }
            else if (formulario.Pdfdoc.value.length == 0 && formulario.validador.value == "false") {
                alert("No puedes remplazar un archivo existente con uno vacio");
                condicion = false;
            }
        }
        if (condicion) { condicion = confirm('Quieres actualizar el registro?'); }
    } else {
        if (formulario.Pdfdoc.value.length != 0 && formulario.validador.value == "false") {
            alert("El archivo debe ser validado");
            condicion = false;
        }
        else if (formulario.Pdfdoc.value.length == 0 && formulario.validador.value == "false") {
            alert("No se puede cargar un archivo uno vacio");
            condicion = false;
        }
        if (condicion) { condicion = confirm('Deseas registar esta entrada?'); }
    }

    if (!condicion) {
        if (evt.preventDefault) { event.preventDefault(); }
        else if (evt.returnValue) { evt.returnValue = false; }
        else { return false; }
    }
}

//function createXMLHttp() {
//    if (typeof XMLHttpRequest != "undefined") {
//        return new XMLHttpRequest();
//    } else if (window.ActiveXObject) {
//        var aVersions = ["MSXML2.XMLHttp.5.0", "MSXML2.XMLHttp.4.0", "MSXML2.XMLHttp.3.0", "MSXML2.XMLHttp", "Microsoft.XMLHttp"];
//        for (var i = 0; i < aVersions.length; i++) {
//            try {
//                var oXmlHttp = new ActiveXObject(aVersions[i]);
//                return oXmlHttp;
//            } catch (oError) {
//                //void
//            }
//        }
//    }
//    throw new Error("XMLHttp object could not be created.");
//}

function cargarFile() {
    document.getElementById("respuesta").innerHTML = "Cargando archivo, espere .....";

    var formulario = document.getElementsByName("FomuValidar")[0]; //PODEMOS OBTENER EL FORMULARIO POR NOMBRE O ID, SI LA 2ND ENTONCES USAR GETELEMENTID

    var key = formulario.elements["__RequestVerificationToken"].value;
    var asd = formulario.elements["Pdfdoc"].files[0];

    var formdata = new FormData();

    //var key = document.getElementsByName('__RequestVerificationToken')[1].value;

    formdata.append("Pdfdoc", document.getElementById("Pdfdoc").files[0]);
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "/Ajax/ValidarArchivo", true);
    xhr.setRequestHeader("RequestVerificationToken",key);
    //xhr.setRequestHeader("Content-Type", "multipart/form-data");
    xhr.send(formdata);

    xhr.onload = function () {
        if (this.status == 200) {
            var rpt = xhr.responseText;
            var rpta = JSON.parse(rpt);
            var span = rpta.span;
            var vali = rpta.bool;
            document.getElementById("respuesta").innerHTML = span;
            //if (rpt == "") {
            document.getElementById("validador").value = vali
            //}
        }
    }
}