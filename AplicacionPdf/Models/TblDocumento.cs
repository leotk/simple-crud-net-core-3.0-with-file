﻿using AplicacionPdf.ViewObject;
using System;
using System.ComponentModel.DataAnnotations;


namespace AplicacionPdf.Models
{
    public class TblDocumento
    {
        [Key]
        public int IdDocu { get; set; }
        public String Numero { get; set; }
        public String Nombre { get; set; }
        public Byte[] Documento { get; set; }

        public TblDocumento(DocFile doc)
        {
            Numero = doc.Numero;
        }

        public TblDocumento()
        {
        }
    }
}
