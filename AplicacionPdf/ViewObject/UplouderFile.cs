﻿using AplicacionPdf.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace AplicacionPdf.ViewObject
{
    public class UplouderFile /*: ControllerBase*/
    {
        private static string[] AdmitedExtens = { ".qwe", ".pdf" };

        //private readonly DataContext ctx;

        //public UplouderFile(DataContext _ctx)
        //{
        //    ctx = _ctx;
        //}

        public static byte[] GetByteArray(IFormFile file)
        {
            using (var target = new MemoryStream())
            {
                file.CopyTo(target);
                return target.ToArray();
            }
        }

        public static Boolean PermitedExten(IFormFile file) {

            bool rpa = true;

            //var nombre = WebUtility.HtmlEncode(file.Name.Substring(file.Name.IndexOf(".",
            //        StringComparison.Ordinal) + 1));

            var ext = Path.GetExtension(file.FileName).ToLowerInvariant();

            if (!AdmitedExtens.Contains(ext))
            {
                rpa = false;
            }

            return rpa;
        }

        public static Boolean FileSize(IFormFile file, long fileSizeLimit)
        {
            bool rpa = true;

            if (file.Length > fileSizeLimit)
            {
                rpa = false;
            }

            return rpa;
        }

    }
}
