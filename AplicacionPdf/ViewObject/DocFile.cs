﻿using AplicacionPdf.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AplicacionPdf.ViewObject
{
    public class DocFile
    {
        public DocFile(){}

        public DocFile(TblDocumento doc)
        {
            Id = doc.IdDocu;
            Numero = doc.Numero;
            Nombre = doc.Nombre;
        }

        public int Id { get; set; }
        public string Numero { get; set; }
        public String Nombre { get; set; }
        public IFormFile Pdfdoc { get; set; }
    }
}
