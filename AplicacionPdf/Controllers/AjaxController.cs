﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AplicacionPdf.ViewObject;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace AplicacionPdf.Controllers
{
    public class AjaxController : Controller
    {
        private long fileSizeLimit = 2097152;
        //public IActionResult Index()
        //{
        //    return View();
        //}

        [HttpPost]
        [ValidateAntiForgeryToken]
        public string ValidarArchivo()
        {
            Dictionary<string, string> valores = new Dictionary<string, string>();
            String valor, span = "";

            try
            {
                IFormFile file = Request.Form.Files[0];
                    if (file.Length != 0)
                    {
                        if (!UplouderFile.PermitedExten(file))
                        {
                            span = "<span style=\"color:red\">Este tipo de archivo no es permitido</span>";
                            valores.Add("span", span);
                            valores.Add("bool", "false");
                        }
                        else if (!UplouderFile.FileSize(file, fileSizeLimit))
                        {
                            //var megaby = fileSizeLimit / 1048576;
                            //span = "<span style=\"color:red\">El archivo excede el maximo de 2MB por: "+ megaby + "MB</span>";
                            span = "<span style=\"color:red\">El archivo excede el maximo de 2MB </span>";
                            valores.Add("span", span);
                            valores.Add("bool", "false");
                        }
                        else
                        {
                            span = "<span style=\"color:green\">El archivo se cargo correctamente</span>";
                            valores.Add("span", span);
                            valores.Add("bool", "true");
                        }
                    }
                    else
                    {
                        span = "<span style=\"color:red\">Error no se puede cargar un archivo vacio</span>";
                        valores.Add("span", span);
                        valores.Add("bool", "false");
                    }
            }
            catch (Exception ex)
            {
                span = "<span style=\"color:red\">Error no se cargo ningun archivo </span>";
                valores.Add("span", span);
                valores.Add("bool", "false");
            }

            valor = JsonConvert.SerializeObject(valores, Formatting.Indented);

            return valor;
        }

        //[HttpPost]
        //public IActionResult ValidarArchivo()
        //{
        //    Console.WriteLine("OnPostGeoLocation CALLED ####################################");

        //    return new JsonResult("OnPostGeoLocation CALLED ####################################");
        //}


    }
}