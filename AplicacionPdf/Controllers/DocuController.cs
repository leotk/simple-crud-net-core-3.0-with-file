﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AplicacionPdf.Models;
using AplicacionPdf.ViewObject;
using iTextSharp.text.pdf;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace AplicacionPdf.Controllers
{
    public class DocuController : Controller
    {
        private readonly DataContext ctx;

        public DocuController(DataContext _ctx)
        {
            ctx = _ctx;
        }

        public IActionResult Index()
        {
            ViewBag.Datos = ctx.TblDocumento.ToList();
            DocFile docx = new DocFile();
            if (TempData["PdfEdit"] != null)
            {
                //var docx = TempData["PdfEdit"] as DocFile;
                docx = JsonConvert.DeserializeObject<DocFile>((String)TempData["PdfEdit"]);
                ViewBag.Editar = true;
            }
            return View(docx);
        }


        [BindProperty]
        public DocFile Docfile { get; set; }
        //[HttpPost]
        public IActionResult NewFile(String Typeaction, int Id)
        {
            String action = Typeaction;

            if (action.Equals("PasData"))
            {
                var Docu = ctx.TblDocumento.Find(Id);
                var FileDoc = new DocFile(Docu);
                TempData["PdfEdit"] = JsonConvert.SerializeObject(FileDoc);
            }
            else if (action.Equals("Create"))
            {
                if (!ModelState.IsValid)
                {
                    return View("~/Views/Docu/Index.cshtml");
                }
                var Copdf = new TblDocumento(Docfile);
                if (Docfile.Pdfdoc != null)
                {
                    Copdf.Nombre = Docfile.Pdfdoc.FileName;
                    Copdf.Documento = UplouderFile.GetByteArray(Docfile.Pdfdoc);
                }
                ctx.TblDocumento.Add(Copdf);
            }            
            else if (action.Equals("Update"))
            {
                var TblDoc = ctx.TblDocumento.Find(Docfile.Id);
                TblDoc.Numero = Docfile.Numero;
                if (Docfile.Pdfdoc != null)
                {
                    TblDoc.Nombre = Docfile.Pdfdoc.FileName;
                    TblDoc.Documento = UplouderFile.GetByteArray(Docfile.Pdfdoc);
                }
            }            
            else if (action.Equals("Delete"))
            {
               var Documento =  ctx.TblDocumento.FirstOrDefault(i => i.IdDocu == Id);
                ctx.TblDocumento.Remove(Documento);
            }            

            ctx.SaveChanges();
            return RedirectToAction("Index");
        }

        public IActionResult DownloadFile(int Id)
        {
            var Docu =  ctx.TblDocumento.Find(Id);
            if (Docu.Documento != null)
            {
                string fileName = Docu.Nombre;
                return File(Docu.Documento, "application/octet-Strem", fileName);
            }
            return Redirect("/Docu/");
            /*
            System.IO.File.ReadAllBytes(filePath);
            return File(Docu,System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
            For preview pdf and the download it use below code
             var stream = new FileStream(filePath, FileMode.Open);
            return new FileStreamResult(stream, "application/pdf");
            */
        }

    }
}